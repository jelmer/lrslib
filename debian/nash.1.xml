<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<refentry id='nash'>
<refmeta>
<refentrytitle>nash</refentrytitle>
<manvolnum>1</manvolnum>
<refmiscinfo class='source'>July 2009</refmiscinfo>
<refmiscinfo class='manual'>lrslib 0.42b</refmiscinfo>
</refmeta>
<refnamediv id='name'>
<refname>nash</refname>
<refpurpose>find nash equilibria of  two person noncooperative games</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
<cmdsynopsis>
  <command>setupnash input game1.ine game2.ine</command>    
</cmdsynopsis>
<cmdsynopsis>
  <command>setupnash2 input game1.ine game2.ine</command>    
</cmdsynopsis>

<cmdsynopsis>
  <command>nash game1.ine game2.ine</command>
</cmdsynopsis>

<cmdsynopsis>
  <command>2nash game1.ine game2.ine</command>
</cmdsynopsis>

</refsynopsisdiv>

<refsect1 id='description'><title>DESCRIPTION</title>
  <title>Nash      Equilibria</title>
<para>
  All Nash equilibria (NE) for a two person noncooperative game are
  computed using two interleaved reverse search vertex enumeration
  steps. The input for the problem are
  two m by n matrices A,B of integers or rationals. The first player
  is the row player, the second is the column
  player. If row i and column j are
  played, player 1 receives A<subscript>i,j</subscript> and player 2
  receives B<subscript>i,j</subscript>.
  If you have two or more cpus
  available run 2nash instead of nash as the order of the input games
  is immaterial. It runs in parallel
  with the games in each order. (If you use nash, the program usually
  runs faster if m is &lt;= n , see  below.) The
  easiest way to use the program nash or 2nash is to first run
  setupnash or ( setupnash2 see below ) on a file
  containing:&nbsp;
  <programlisting>
  m n
  matrix A
  matrix B
  </programlisting>
  eg.  the file game is for a game with m=3 n=2:
  <programlisting>
  3 2

  0 6
  2 5
  3 3
  
  1 0
  0 2
  4 3
  </programlisting>
  <programlisting>
  % setupnash game game1 game2
  </programlisting>
  produces
  two H-representations, game1 and game2, one for each
  player. To  get the equilibria,
  run
  <programlisting>
  %  nash game1  game2
  </programlisting>
or
  <programlisting>
  %  2nash game1  game2
  </programlisting>
</para>
<para>
  Each row beginning 1 is a strategy
  for the row player yielding a NE with each row beginning 2 listed
  immediately above it.The payoff for
  player 2 is the last number on the line beginning 1, and vice
  versa. Eg: first two lines of
  output: player 1 uses row probabilities 2/3 2/3 0 resulting in a
  payoff of 2/3 to player 2.Player 2
  uses column probabilities 1/3 2/3 yielding a payoff of 4 to player
  1.
  If both matrices are nonnegative and
  have no zero columns, you may instead use
  setupnash2:
 <programlisting>
  % setupnash2 game game1 game2
</programlisting>
Now
  the polyhedra produced are polytopes.
  The
  output&nbsp; of nash in this case is a list of unscaled probability
  vectors x and  y.  To
  normalize, divide each vector by v = 1^T x and u=1^T
  y.u and v are the payoffs to players
  1 and 2
  respectively.  In
  this case, lower bounds on the payoff functions to either or both
  players may be included. To give a
  lower bound of r on the payoff for player 1 add the options to file
  game2&nbsp; (yes that is correct!)To
  give a lower bound of r on the payoff for player 2 add the options
  to file game1
 <programlisting>
  minimize
  0 1 1 ... 1 &nbsp;&nbsp; (n entries to begiven)
  bound &nbsp; 1/r;&nbsp;&nbsp;  ( note: reciprocal of r)
</programlisting>
  If you do not wish to use the 2-cpu program 2nash, please read the
  following. If
  m is greater than n then nash usually runs faster by transposing
  the players. This is achieved by
  running: 
 <programlisting>
 %  nash game2  game1
</programlisting>
If you wish to construct the game1 and game2 files by hand, see the 
<ulink url="#Nash%20Equilibria">lrslib user manual</ulink>
</para>
</refsect1>
<refsect1><title>SEE ALSO</title>
<para>
For information on <emphasis>H-representation</emphasis> file formats, see the man page for lrslib or the <ulink url="#File%20Formats">lrslib user manual</ulink>

</para>
</refsect1>
</refentry>

